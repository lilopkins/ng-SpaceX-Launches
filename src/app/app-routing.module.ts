import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SpacexLaunchOverviewComponent } from './spacex-launch-overview/spacex-launch-overview.component';
import { SpacexLaunchDetailComponent } from './spacex-launch-detail/spacex-launch-detail.component';

const routes: Routes = [
    { path: '', component: SpacexLaunchOverviewComponent },
    { path: 'detail/:id', component: SpacexLaunchDetailComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
