import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

const API_URLS = {
    'ALL': 'https://api.spacexdata.com/v2/launches',
    'ONE': 'https://api.spacexdata.com/v2/launches?flight_number={id}'
};

@Injectable()
export class SpacexLaunchService {

  constructor(private http: Http) { }

  fetchAll(): Observable<any> {
      return this.http.get(API_URLS.ALL).map(response => response.json());
  }

  fetch(id: number): Observable<any> {
      return this.http.get(API_URLS.ONE.replace('{id}', id.toString())).map(response => response.json()[0]);
  }

}
