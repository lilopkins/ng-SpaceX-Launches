import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpacexLaunchOverviewComponent } from './spacex-launch-overview.component';

describe('SpacexLaunchOverviewComponent', () => {
  let component: SpacexLaunchOverviewComponent;
  let fixture: ComponentFixture<SpacexLaunchOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpacexLaunchOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpacexLaunchOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
