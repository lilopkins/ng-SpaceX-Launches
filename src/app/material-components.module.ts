import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatPaginatorModule } from '@angular/material/paginator';

const mods = [
  MatTableModule,
  MatButtonModule,
  MatExpansionModule,
  MatProgressBarModule,
  MatPaginatorModule
];

@NgModule({
  imports: mods,
  exports: mods
})
export class MaterialComponentsModule { }
